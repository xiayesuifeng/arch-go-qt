FROM archlinux/archlinux:base-devel

ENV CGO_CXXFLAGS_ALLOW=".*"
ENV CGO_LDFLAGS_ALLOW=".*"
ENV CGO_CFLAGS_ALLOW=".*"

RUN  pacman -Sy --noconfirm go qt5 git \
    && tar -xf /var/cache/pacman/pkg/qt5-doc-*-any.pkg.tar.zst -C / \ 
    && yes | pacman -Scc \
    && export GOPATH=~/go \
    && export PATH=$PATH:~/go/bin \
    && export QT_PKG_CONFIG=true \
    && export GO111MODULE=off \
    && go get -u -tags=no_env github.com/therecipe/qt/cmd/... \
    && qtsetup generate \
    && qtsetup install
